﻿CREATE DATABASE IF NOT EXISTS ejemplo3Yii;
USE ejemplo3Yii;

CREATE OR REPLACE TABLE naciones(
  id int AUTO_INCREMENT,
  victorias int,
  nombre varchar(50),
  continente varchar(50),
  PRIMARY KEY (id)
  );

CREATE OR REPLACE TABLE tenistas(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  correo varchar(50),
  activo bool,
  fechaBaja date,
  fechaNacimiento date,
  altura int,
  peso int,
  nacion int,
  PRIMARY KEY (id),
  CONSTRAINT fktenistaNacion FOREIGN KEY (nacion) 
    REFERENCES naciones(id) ON DELETE CASCADE on UPDATE CASCADE
);

